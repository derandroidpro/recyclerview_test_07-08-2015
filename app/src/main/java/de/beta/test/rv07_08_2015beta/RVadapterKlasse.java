package de.beta.test.rv07_08_2015beta;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class RVadapterKlasse extends RecyclerView.Adapter <RVadapterKlasse.ViewHolderKlasse> {

    public class ViewHolderKlasse extends RecyclerView.ViewHolder{
        TextView itemtextview;
        ImageView itemimageView;

        public ViewHolderKlasse(View itemView) {
            super(itemView);
            itemtextview = (TextView) itemView.findViewById(R.id.itemtextView);
            itemimageView = (ImageView) itemView.findViewById(R.id.itemimageView);

        }
    }

    @Override
    public ViewHolderKlasse onCreateViewHolder(ViewGroup viewGroup, int i) {

        View itemView1 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_layout1, null);


        return new ViewHolderKlasse(itemView1);
    }

    @Override
    public void onBindViewHolder(ViewHolderKlasse viewHolderKlasse, final int i) {

        viewHolderKlasse.itemtextview.setText(MainActivity.itemtexte.get(i));
        viewHolderKlasse.itemimageView.setImageResource(MainActivity.itemIconIDs.get(i));

        viewHolderKlasse.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MainActivity.textView.setText(MainActivity.itemtexte.get(i));

            /*
                switch (i){

                    case 0:
                        // your code
                        break;
                    case 1:
                        // your code
                        break;
                }   */
            }
        });

    }

    @Override
    public int getItemCount() {
        return MainActivity.itemtexte.size();
    }
}
