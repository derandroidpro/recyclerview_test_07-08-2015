package de.beta.test.rv07_08_2015beta;

import android.app.Activity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView1;
    RecyclerView.Adapter rvadapter1;
    RecyclerView.LayoutManager rvlayoutManager1;

   static ArrayList<String> itemtexte;
   static ArrayList<Integer> itemIconIDs;

    public static TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        itemtexte = new ArrayList<>();
        itemIconIDs = new ArrayList<>();
        itemtexte.addAll(Arrays.asList("Item 1", "Item 2", "Item 3"));
        itemIconIDs.addAll(Arrays.asList(
                 R.drawable.ic_access_alarm_grey600_18dp
                ,R.drawable.ic_accessibility_grey600_18dp
                ,R.drawable.ic_account_box_grey600_18dp
        ));

        recyclerView1 = (RecyclerView) findViewById(R.id.recyclerview1);

        rvlayoutManager1 = new LinearLayoutManager(this);
        recyclerView1.setLayoutManager(rvlayoutManager1);

        rvadapter1 = new RVadapterKlasse();
        recyclerView1.setAdapter(rvadapter1);
        recyclerView1.setHasFixedSize(true);

        textView = (TextView) findViewById(R.id.textView);
    }


}
